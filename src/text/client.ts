import { LanguageType } from "../common/language";
import { TenbyteConfig } from "../conf";
import { TenbyteClient } from "../http/client";
import { TenbyteClientOptions } from "../http/interfaces";
import { TextAutocompleteResponseElement, TextSynonymsResponseElement } from "./interfaces";


export class TextClient extends TenbyteClient {

    constructor(client: TenbyteClient | TenbyteClientOptions) {
        super(client);
    }


    public async autocomplete(
        lang: LanguageType,
        trade: string,
        mode: 'prefix' | 'prefix_any' | 'match' = 'prefix',
        max: number = 5
    ): Promise<TextAutocompleteResponseElement> {
        const res = await this.post(TenbyteConfig.routes.text.autocomplete + lang.toLowerCase(), { trade, mode, max });

        return res.body.payload;
    }

    public async synonyms(
        lang: LanguageType,
        trade: string,
        bias: number = 0,
        variance: number = 0,
        max: number = 25
    ): Promise<TextSynonymsResponseElement> {
        const res = await this.post(TenbyteConfig.routes.text.synonyms + lang.toLowerCase(), { trade, bias, variance, max });

        return res.body.payload;
    }
}