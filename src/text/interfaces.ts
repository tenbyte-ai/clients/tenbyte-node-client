export interface TextAutocompleteSuggestion {
    text: string;
    score: number;
    categories: 'servcice' | 'construction' | 'delivery' | 'maintenance'[];
}


export interface TextAutocompleteResponseElement {
    data: string;
    suggestions: TextAutocompleteSuggestion[];
}


export interface TextSynonym {
    text: string;
    deviation: number;
}


export interface TextSynonymsResponseElement {
    data: string;
    synonyms: TextSynonym[][];
    additional: number[];
}
