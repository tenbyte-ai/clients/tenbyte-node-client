export const TenbyteConfig = {
    version: '2.1.0',
    urls: {
        test: ['https://api1-test.tenbyte.ai/v1/', 'https://api2-test.tenbyte.ai/v1/', 'https://api3-test.tenbyte.ai/v1/'],
        live: ['https://api1.tenbyte.ai/v1/', 'https://api2.tenbyte.ai/v1/', 'https://api3.tenbyte.ai/v1/'],
    },
    headers: [
        { key: 'Content-Type', value: 'application/json' },
        { key: 'Accept', value: 'application/json' }
    ],
    routes: {
        auth: 'user/auth',
        cpv: {
            predict: 'ai/cpv/predict/',
            predictMulti: 'ai/cpv/predict-multi-label/',
            tqc: 'ai/cpv/text-quality-check/'

        },
        text: {
            autocomplete: 'ai/text/autocomplete/',
            synonyms: 'ai/text/synonyms/'
        }
    },
    timeout: 5000
}