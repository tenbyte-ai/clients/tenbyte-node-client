import * as agent from "superagent";
import { TenbyteConfig } from "../conf";
import { HttpHeader, TenbyteClientOptions } from "./interfaces";




export class TenbyteClient {
    protected options: TenbyteClientOptions = {};
    protected urls: string[] = [];
    protected url: string = '';
    protected failCount: number[] = [];
    private headers: HttpHeader[] = [];
    private authed = false;


    constructor(options: TenbyteClient | TenbyteClientOptions) {
        if (options instanceof TenbyteClient) {
            Object.assign(this, options);
        } else {
            this.options = options;

            switch (this.options.mode) {
                case 'test':
                    this.urls = TenbyteConfig.urls.test;
                    break;
                default:
                    this.urls = TenbyteConfig.urls.live;
                    break;
            }

            if (!this.options.timeout) {
                this.options.timeout = TenbyteConfig.timeout;
            }



            this.url = this.urls[0];

            this.headers = TenbyteConfig.headers;

            for (const url of this.urls) {
                this.failCount.push(0);
            }
        }
    }

    public async init(): Promise<void> {
        await this.auth();
    }


    public addHeader(header: HttpHeader): void {
        this.headers.push(header);
    }


    public post(path: string, body?: any, additionalHeaders: HttpHeader[] = []): Promise<agent.Response> {
        return new Promise((resolve, reject) => {
            this.try('POST', path, resolve, reject, { body, additionalHeaders });
        });
    }

    public get(path: string, additionalHeaders: HttpHeader[] = []): Promise<agent.Response> {
        return new Promise((resolve, reject) => {
            this.try('GET', path, resolve, reject, { additionalHeaders });
        });
    }

    public isAuthed(): boolean {
        return this.authed;
    }

    protected async auth(): Promise<void> {
        try {
            const res = await this.post(TenbyteConfig.routes.auth, this.options.cred);
            this.addHeader({ key: 'Authorization', value: res.body.payload.token });
            this.authed = true;
        } catch (err) {
            throw new Error('Authentication Error');
        }
    }

    private async try(
        method: 'GET' | 'POST',
        path: string,
        resolve: (response: agent.Response) => void,
        reject: (err: any) => void,
        options: {
            body?: any,
            additionalHeaders?: HttpHeader[],
        } = {},
        cycleUrl: boolean = false
    ): Promise<void> {

        const opt = {
            body: undefined,
            additionalHeaders: []
        };
        Object.assign(opt, options);

        const headers = this.headers.concat(opt.additionalHeaders);


        if (cycleUrl) {
            this.cycleUrl(this.url);
        }

        let request: agent.SuperAgentRequest;

        switch (method) {
            case 'GET':
                request = agent.get(this.url + path);
                break;
            case 'POST':
                request = agent.post(this.url + path).send(opt.body);

                break;
            default:
                throw new Error('Unknown Method: ' + method);
        }

        if (this.options.timeout) {
            request = request.timeout(this.options.timeout);
        } else {
            request = request.timeout(TenbyteConfig.timeout);
        }



        for (const header of headers) {
            request = request.set(header.key, header.value)
        }


        request.end((err, res) => {
            if (err === null) {
                resolve(res);
            }
            else {
                if (err && (err.code || err.status >= 500)) {
                    if (!this.cycledOut(this.url)) {
                        this.try(method, path, resolve, reject, options, true);
                    } else {
                        reject(err)
                    }
                } else {
                    reject(err)
                }
            }
        });
    }

    private indexOf(url: string): number {
        let i = 0;
        for (const baseUrl of this.urls) {
            if (url.search(baseUrl) === 0) {
                return i;
            }
            i++;
        }

        return -1;
    }

    private cycleUrl(url: string): string {
        const i = this.indexOf(url);
        this.persist(i);

        if ((i + 1) < this.urls.length) {
            return url.replace(this.urls[i], this.urls[i + 1]);
        } else {
            return this.urls[0];
        }
    }

    private cycledOut(url: string): boolean {
        return (this.indexOf(url) + 1) === this.urls.length;
    }

    private persist(i: number): void {
        this.failCount[i]++;

        this.url = this.urls[this.failCount.indexOf(Math.min.apply(Math, this.failCount))];
    }
}