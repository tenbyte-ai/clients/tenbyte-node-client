export interface TenbyteClientOptions {
    cred?: {
        email?: string,
        pwd?: string,
        apikey?: string,
    };
    mode?: 'test' | 'live'; // default 'live'
    timeout?: number;
}

export interface HttpHeader {
    key: string;
    value: string;
}