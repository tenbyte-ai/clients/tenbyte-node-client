import { TenbyteClient } from "../http/client";
import { CPVClient } from "../cpv/client";
import { TextClient } from "../text/client";
import { Language } from "../common/language";

let httpClient: TenbyteClient;
let cpvClient: CPVClient;
let textClient: TextClient;


test('Create http client', async () => {
    httpClient = new TenbyteClient({
        mode: 'test',
        cred: {
            email: 'info@tenbyte.ai',
            pwd: 'secret'
        },
        timeout: 10000
    });

    await httpClient.init();
    expect(httpClient.isAuthed()).toBeTruthy()
}, 10000);

test('Create CPV client', async () => {
    cpvClient = new CPVClient(httpClient);

    expect(cpvClient).toBeInstanceOf(CPVClient)
}, 10000);

test('Create Text client', async () => {
    textClient = new TextClient(httpClient);

    expect(textClient).toBeInstanceOf(TextClient)
}, 10000);


test('CPV exec tqc', async () => {
    const tqcRes = await cpvClient.textQualityCheck(Language.de, ['Laptops']);

    const tqcObject = [{ data: 'Laptops', quality: tqcRes[0].quality }];

    expect(tqcRes).toStrictEqual(tqcObject);
}, 10000);

test('CPV exec classifier', async () => {
    const clfRes = await cpvClient.predictMulti(Language.de, ['Laptops'], {hierarchic: true});
    // console.log(clfRes);
    const clfObject = [{ data: 'Laptops', predictions: clfRes[0].predictions }];

    expect(clfRes).toStrictEqual(clfObject);
}, 10000);


test('TEXT exec autocomplete', async () => {
    const acRes = await textClient.autocomplete(Language.de, 'Laptop');
    // console.log(acRes);

    expect(acRes).not.toBeNull();
}, 10000);

test('TEXT exec synonyms', async () => {
    const synRes = await textClient.synonyms(Language.en, 'Laptop');
    // console.log(synRes);
    expect(synRes).not.toBeNull();
}, 10000);