export interface CpvCodePredictionOptions {
    hierarchic?: boolean;
}

export interface CpvCodeHierarchicEntry {
    short: string;
    full: string;
    score: {
        iter: number;
        total: number;
    };
    desc: string;
}

export interface CpvPrediction {
    code: string;
    score: number;
    desc: string;
    hierarchic?: CpvCodeHierarchicEntry[];
    showHierarchic?: boolean;
}

export interface CpvPredictionResponseElement {
    data: string;
    prediction: CpvPrediction;
}

export interface CpvMultiLabelPredictionResponseElement {
    data: string;
    predictions: CpvPrediction[];
}

export interface CpvTextQualityResponseElement {
    data: string;
    quality: number;
}


