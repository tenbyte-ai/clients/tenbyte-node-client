
import { LanguageType } from "../common/language";
import { TenbyteConfig } from "../conf";
import { TenbyteClient } from "../http/client";
import { TenbyteClientOptions } from "../http/interfaces";
import { CpvPredictionResponseElement, CpvTextQualityResponseElement, CpvMultiLabelPredictionResponseElement, CpvCodePredictionOptions } from "./interfaces";


export class CPVClient extends TenbyteClient {

    constructor(client: TenbyteClient | TenbyteClientOptions) {
        super(client);
    }

    public async predict(lang: LanguageType, trades: string[], options?: CpvCodePredictionOptions): Promise<CpvPredictionResponseElement[]> {
        const res = await this.post(TenbyteConfig.routes.cpv.predict + lang.toLowerCase(), { trades, options });

        return res.body.payload;
    }

    public async predictMulti(lang: LanguageType, trades: string[], options?: CpvCodePredictionOptions): Promise<CpvMultiLabelPredictionResponseElement[]> {
        const res = await this.post(TenbyteConfig.routes.cpv.predictMulti + lang.toLowerCase(), { trades, options });

        return res.body.payload;
    }

    public async textQualityCheck(lang: LanguageType, trades: string[]): Promise<CpvTextQualityResponseElement[]> {
        const res = await this.post(TenbyteConfig.routes.cpv.tqc + lang.toLowerCase(), { trades });

        return res.body.payload;
    }
}