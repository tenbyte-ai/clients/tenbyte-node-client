import { TenbyteClient } from "../http/client";


export type LanguageType =
    'cs' |
    'da' |
    'de' |
    'en' |
    'es' |
    'et' |
    'fi' |
    'fr' |
    'hu' |
    'it' |
    'lt' |
    'nl' |
    'pl' |
    'pt' |
    'ro' |
    'sk' |
    'sl' |
    'sv';

export class Language {
    public static cs: LanguageType = 'cs';
    public static da: LanguageType = 'da';
    public static de: LanguageType = 'de';
    public static en: LanguageType = 'en';
    public static es: LanguageType = 'es';
    public static et: LanguageType = 'et';
    public static fi: LanguageType = 'fi';
    public static fr: LanguageType = 'fr';
    public static hu: LanguageType = 'hu';
    public static it: LanguageType = 'it';
    public static lt: LanguageType = 'lt';
    public static nl: LanguageType = 'nl';
    public static pl: LanguageType = 'pl';
    public static pt: LanguageType = 'pt';
    public static ro: LanguageType = 'ro';
    public static sk: LanguageType = 'sk';
    public static sl: LanguageType = 'sl';
    public static sv: LanguageType = 'sv';
}
