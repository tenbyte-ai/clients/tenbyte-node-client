# tenbyte-node-client

## Installation

```bash
  npm i --save @tenbyte.ai/node-client
```

## Usage

Register and activate an account on <https://tenbyte.ai>.

### Create and init a new http Client

```ts
import { TenbyteClient } from "@tenbyte.ai/node-client";

const httpClient = new TenbyteClient({
  cred: {
    email: "mail@example.com",
    pwd: "secret",
    // or
    apikey: "api-key-123"
  },
});

// Promise
httpClient
  .init()
  .then(() => {
    httpClient.isAuthed(); // => boolean
    // the http client has authed successfully and is ready for use
  })
  .catch((err) => {
    // exception handling here
  });

// async / await
await httpClient.init();
httpClient.isAuthed(); // => boolean
```

### Create a new CPV client and request a classification

```ts
import { CPVClient, Language } from "@tenbyte.ai/node-client";
const cpvClient = new CPVClient(httpClient);

// Promise
cpvClient
  .predict(Language.en, ["Laptops"])
  .then((clfRes: CpvPredictionResponseElement) => {
    // handle the prediction result
  })
  .catch((err) => {
    // exception handling here
  });

// async / await
let clfRes: CpvPredictionResponseElement = await cpvClient.predictMulti(Language.de, ["Laptops"], { hierarchic: true });
```

### Create a new text client and request an autocomplete

```ts
import { TextClient, Language } from "@tenbyte.ai/node-client";
const textClient = new TextClient(httpClient);

// Promise
textClient
  .autocomplete(Language.en, "Laptops")
  .then((res: TextAutocompleteResponseElement) => {
    // handle the prediction result
  })
  .catch((err) => {
    // exception handling here
  });

// async / await
let res: TextAutocompleteResponseElement = await textClient.autocomplete(Language.de, ["Laptops"]);
```

## TenbyteClient

### Methods [tenbyte-client-methods]

#### init()

```ts
TenbyteClient.init(): Promise<void>
```

#### addHeader(header: HttpHeader)

```ts
TenbyteClient.addHeader(
  header: HttpHeader
): void
```

#### post(path: string, body?: any, additionalHeaders: HttpHeader[] = [])

```ts
TenbyteClient.post(
  path: string,           // full path including BaseURL
  body?: any, 
  additionalHeaders: HttpHeader[] = []
): Promise<superagent.Response>
```

#### get(path: string, additionalHeaders: HttpHeader[] = [])

```ts
TenbyteClient.get(
  path: string,           // full path including BaseURL
  additionalHeaders: HttpHeader[] = []
): Promise<superagent.Response>
```

#### isAuthed()

```ts
TenbyteClient.isAuthed(): boolean
```

## CPVClient

### Methods [cpv-client-methods]

#### predict(lang: LanguageType, trades: string[])

```ts
CPVClient.predict(
  lang: LanguageType, 
  trades: string[]
): Promise<CpvPredictionResponseElement[]>

```

#### predictMulti(lang: LanguageType, trades: string[])

```ts
CPVClient.predictMulti(
  lang: LanguageType,   // use Language object to define the requested language
  trades: string[] 
): Promise<CpvPredictionResponseElement[]>

```

#### textQualityCheck(lang: LanguageType, trades: string[])

```ts
CPVClient.textQualityCheck(
  lang: LanguageType, 
  trades: string[]
): Promise<CpvTextQualityResponseElement[]>
```

## TextClient

### Methods [text-client-methods]

#### autocompete(lang: LanguageType, trade: string)

```ts
TextCleint.autocompete(
  lang: LanguageType, 
  trades: string[]
): Promise<TextAutocompleteResponseElement[]>

```

#### synonyms(lang: LanguageType, trade: string)

```ts
TextCleint.synonyms(
  lang: LanguageType,   // use Language object to define the requested language
  trade: string
): Promise<TextSynonymsResponseElement[]>

```
